"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var ng2_pagination_1 = require("ng2-pagination"); //importing ng2-pagination
var core_2 = require("angular2-google-maps/core"); //AgmCoreModule for google maps
// Imports for loading & configuring the in-memory web api
var angular_in_memory_web_api_1 = require("angular-in-memory-web-api");
var in_memory_data_service_1 = require("./services/in-memory-data.service");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var company_search_component_1 = require("./components/company-search.component");
var company_detail_component_1 = require("./components/company-detail.component");
var company_service_1 = require("./services/company.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule,
            angular_in_memory_web_api_1.InMemoryWebApiModule.forRoot(in_memory_data_service_1.InMemoryDataService),
            app_routing_module_1.AppRoutingModule, ng2_pagination_1.Ng2PaginationModule,
            core_2.AgmCoreModule.forRoot({ apiKey: 'AIzaSyA6iMuRcOz6qj_zxEa_Xjp0WRjrcM1qtrQ' })
        ],
        declarations: [app_component_1.AppComponent, company_detail_component_1.CompanyDetailComponent, company_search_component_1.CompanySearchComponent],
        providers: [company_service_1.CompanyService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map