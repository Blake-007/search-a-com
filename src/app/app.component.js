"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof router_1.NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        template: "\n<nav class=\"navbar navbar-inverse\" role=\"navigation\">\n    <div class=\"container-fluid\">\n    <div class=\"navbar-header\">\n      <a class=\"navbar-brand\" routerLink=\"/\">Search A Com</a>\n    </div>\n    <ul class=\"nav navbar-nav\">\n      <li><a routerLink=\"/\">Home</a></li>\n      <li><a href=\"#\">About Us</a></li>\n      <li><a href=\"#\">Contact Us</a></li>\n    </ul>\n    </div>\n\n  </nav>\n  <router-outlet></router-outlet>",
        styles: [".navbar \n            {\n                padding-top: 15px;\n                padding-bottom: 15px;\n                border: 0;\n                border-radius: 0;\n                margin-bottom: 0;\n                font-size: 12px;\n                letter-spacing: 5px;\n            }\n\n        .navbar-nav li a:hover \n        {\n            color: white !important;\n        }\n        body \n        {\n            font: 20px Montserrat, sans-serif;\n            line-height: 1.8;\n            color: #f5f6f7;\n        }"]
    }),
    __metadata("design:paramtypes", [router_1.Router])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map