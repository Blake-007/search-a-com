"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
require("rxjs/add/operator/switchMap");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var company_service_1 = require("../services/company.service");
var CompanyDetailComponent = (function () {
    function CompanyDetailComponent(companyService, route) {
        this.companyService = companyService;
        this.route = route;
    }
    CompanyDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.companyService.getCompany(+params['id']); }) //retrieving company info based on ID passed via ActivatedRoute from search component
            .subscribe(function (company) { return _this.company = company; });
    };
    return CompanyDetailComponent;
}());
CompanyDetailComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'detail',
        templateUrl: "company-detail.component.html",
        styleUrls: ["company-detail.component.css"]
    }),
    __metadata("design:paramtypes", [company_service_1.CompanyService, router_1.ActivatedRoute])
], CompanyDetailComponent);
exports.CompanyDetailComponent = CompanyDetailComponent;
//# sourceMappingURL=company-detail.component.js.map