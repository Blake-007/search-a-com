# Explanation of Project Structure # 

Files located in src contains the main code for the site. The src folder contains code that I have reused and a folder called app.

**The files in the src folder that I have modified:**

* index.html

### Files that I have reused and modified include. These files are located in the app folder: ###

* app.component.ts
* app.module.ts

### Files that I have created: ###

**Located in app folder**

* app-routing.module.ts
* company.ts
* app.component.html
* app.component.css

**Located in src/app folder:**

App Folder contains two other folders, Components and Services. I have created all the Files in these folders

**Files in Components Folder**

* company-detail.component.ts
* company-detail.component.css
* company-detail.component.html
* company-search.component.ts
* company-search.component.html
* company-search.component.css
* company.component.ts

**Files in Services Folder**

* company-search.service.ts
* company.service.ts
* in-memory-data.service.ts